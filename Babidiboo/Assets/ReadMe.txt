2018-05-07

Controles:

	W: Avancer
	S: Reculer:
	D: D�placement droit
	A: D�placement gauche
	R: Recommencer le niveau
	N: Niveau suivant (next) *Pas Impl�ment�*

	Space: Saut
	Clic-gauche: Tirer

But du jeu:

	- Tuer tous les ennemis (cube rouge) avant de toucher l'objectif de fin de niveau (Cylindre vert)
	- R�aliser le niveau le plus vite possible 

Bugs:

	- Le joueur ne conserve pas sa v�locit� lorsque dans les airs. 
	- Les grids de floor on tous un Collider et le joueur coche parfois m�me si elles sont toutes bien align�. Il faudrait les regrouper et leur faire partager un m�me collider par groupe. 
	- Le nombre d'ennemi restant n'est pas le bon dans l'affichage de fin de niveau. (Pas pressant, facile � corriger)
	- Les projectiles des ennemis sont trop petits.
	- Les ennemis ne fixent pas TOUJOURS le joueur. � cause d'un collider trigger qui pr�vient le raycast de l'ennemi de d�tecter le joueur.
	- Les colliders du bat�ment de fin de niveau ne sont pas faits.
	- Si le joueur regarde vers le ciel, il y a des risques de probl�me de d�tection du CheckGround (raycasts).

� faire:

	- Ajouter un deuxieme raycast pour que l'ennemi n'attaque pas le joueur lorsquil y a un mur devant lui.  OU ajouter un autre layermask sur les murs.
	- Ajouter 