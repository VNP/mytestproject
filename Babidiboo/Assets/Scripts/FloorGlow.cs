﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloorGlow : MonoBehaviour
{

    [SerializeField]
    private MeshRenderer m_FloorMeshRenderer;

    [SerializeField]
    private Material m_ActivateFloorMat;
    [SerializeField]
    private Material m_NormalFloorMat;

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player")
        {
            m_FloorMeshRenderer.material = m_ActivateFloorMat;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            StartCoroutine(StopGlowing());
        }
    }

    private IEnumerator StopGlowing()
    {
        yield return new WaitForSeconds(1f);
        m_FloorMeshRenderer.material = m_NormalFloorMat;
    }
}
