﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile_VFX : MonoBehaviour
{
    private float m_CurrentTime;
    private float m_EndTime = 2f;

    private void Start()
    {
        m_CurrentTime = Time.time;
        m_EndTime += Time.time;
    }

    private void Update()
    {
        m_CurrentTime += Time.deltaTime;

        if(m_CurrentTime >= m_EndTime)
        {
            Destroy(this.gameObject);
        }
    }

}
