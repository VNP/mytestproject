﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameOverBtns : MonoBehaviour
{
    [SerializeField] private GameObject m_BtnNext;
    public string m_LevelToLoad = "";

    [SerializeField] PlayerController m_PlayerScript;


    private void Update()
    {
        OnPressN_Next();
    }

    private void OnEnable()
    {
        if(GameManager.Instance.areAllEnemiesDead)
        {
            m_BtnNext.SetActive(true);
        }
    }

    public void OnClick_Next()
    {
        Time.timeScale = 1;

        GameManager.Instance.SetInGameMouse();
        GameManager.Instance.ClearLists();

        SceneManager.LoadScene(m_LevelToLoad);

        m_BtnNext.SetActive(false);
    }

    private void OnPressN_Next()
    {
        if (Input.GetKey(KeyCode.N))
        {
            OnClick_Next();
        }
    }

    public void OnClick_Replay()
    {
        Time.timeScale = 1;

        GameManager.Instance.SetInGameMouse();

        m_PlayerScript.RestartLevel();

        m_BtnNext.SetActive(false);
    }
}
