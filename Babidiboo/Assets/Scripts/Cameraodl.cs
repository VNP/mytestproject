﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cameraold : MonoBehaviour
{
    [SerializeField] Vector3 offset;
    [SerializeField] GameObject target;

    private Vector3 cameraPosition;
    private Quaternion cameraRotation;
    

    void Start()
    {
        cameraPosition = transform.position;
    }

    private void Update()
    {
        SynchToTargetPosition();
        SynchToTargetRotation();
    }

    private void SynchToTargetRotation()
    {
        cameraRotation = target.transform.rotation;
        transform.rotation = cameraRotation;
    }

    private void SynchToTargetPosition()
    {
        cameraPosition = target.transform.position + offset;
        transform.position = cameraPosition;
    }
}
