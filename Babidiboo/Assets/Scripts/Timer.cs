﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Timer : MonoBehaviour
{
    [SerializeField] private Text m_Time;

    private float m_CurrentTime;
    private float seconds;

    void Start()
    {
        if (m_Time == null)
        {
            Debug.Log("m_Time has no reference.");
        }
    }

    void Update()
    {
        m_CurrentTime = Time.deltaTime;
        seconds += m_CurrentTime % 60;
        m_Time.text = ("" + (int)seconds);
    }

    public void TimerReset()
    {
        seconds = 0;
    }

    public int GetTime()
    {
        return (int)seconds;
    }
}
