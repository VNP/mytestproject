﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DangerousSphere : MonoBehaviour
{
    private Vector3 m_InitialPosition;

    void Start()
    {
        m_InitialPosition = transform.position; 
    }

    public void MoveToInitialPosition()
    {
        transform.position = m_InitialPosition;
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player")
        {
            PlayerController playerScript = other.GetComponent<PlayerController>();
            playerScript.RestartLevel();
        }
    }
}
