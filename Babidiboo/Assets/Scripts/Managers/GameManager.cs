﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public List<GameObject> m_EnnemiesInTheLevel = new List<GameObject>();
    public List<GameObject> m_FloorsInTheLevel = new List<GameObject>();
    public List<GameObject> m_CloudsInTheLevel = new List<GameObject>();
    public List<GameObject> m_DangerousSpheresInTheLevel = new List<GameObject>();
    public List<GameObject> m_TriggersInTheLevel = new List<GameObject>();
    public List<GameObject> m_Projectiles = new List<GameObject>();

    public PlayerController m_PlayerController;

    public Text m_Timer;
    public Timer m_TimerScript;

    public bool areAllEnemiesDead = false;

    public short remainingEnemies;

    private static GameManager _instance;

    public static GameManager Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = GameObject.FindObjectOfType<GameManager>();
            }
            return _instance;
        }
    }

    private void Start()
    {
        SetInGameMouse();
        remainingEnemies = 0;
    }

    private void Update()
    {
    }

    private void Awake()
    {
        if (_instance == null)
            _instance = this;

        else if (_instance != this)
            Destroy(gameObject);

        DontDestroyOnLoad(gameObject);
    }

    public void SetInGameMouse()
    {
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.lockState = CursorLockMode.Confined;
    }

    public void SetMenuMouse()
    {
        Cursor.visible = true;
    }

    public void ClearLists()
    {
        m_EnnemiesInTheLevel.Clear();
        m_CloudsInTheLevel.Clear();
        m_DangerousSpheresInTheLevel.Clear();
        m_TriggersInTheLevel.Clear();
        m_Projectiles.Clear();
    }

    public int GetRemainingEnnemies()
    {
        int count = 0;

        foreach(GameObject obj in m_EnnemiesInTheLevel)
        {
            if(obj.activeSelf)
            {
                count++;
            }
        }
        return count;
    }
}
