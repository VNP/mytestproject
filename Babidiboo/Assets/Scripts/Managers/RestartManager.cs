﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RestartManager : MonoBehaviour
{

    private static RestartManager _instance;

    public static RestartManager Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = GameObject.FindObjectOfType<RestartManager>();
            }
            return _instance;
        }
    }

    private void Awake()
    {
        if (_instance == null)
            _instance = this;

        else if (_instance != this)
            Destroy(gameObject);

        DontDestroyOnLoad(gameObject);
    }

    /// <summary>
    /// Reset all the elements of the level to its origin state and position.
    /// </summary>
    public void RestartLevel()
    {
        ResetFloors();
        ResetEnnemies();
        ResetTriggers();
        ResetDangerousSpheres();
        ResetClouds();
        HideProjetileS();
        ResetTimer();

        PRManager.Instance.HidGameOverUI();
    }

    private void ResetFloors()
    {
        foreach (GameObject floor in GameManager.Instance.m_FloorsInTheLevel)
        {
            Floor floorScript = floor.GetComponent<Floor>();
            floorScript.ResetMaterial();
        }
    }

    /// <summary>
    /// Enable all ennemies in the current level.
    /// </summary>
    private void ResetEnnemies()
    {
        foreach (GameObject enemy in GameManager.Instance.m_EnnemiesInTheLevel)
        {
            enemy.SetActive(false);
        }

        foreach (GameObject enemy in GameManager.Instance.m_EnnemiesInTheLevel)
        {
            enemy.SetActive(true);

            MeshRenderer mR = enemy.GetComponent<MeshRenderer>();
            Collider bX = enemy.GetComponent<Collider>();
            Enemy enemyScript = enemy.GetComponent<Enemy>();

            mR.enabled = true;
            bX.enabled = true;
            enemyScript.enabled = true;

            enemyScript.m_IsAlive = true;
            enemyScript.m_IsOnCooldown = false;
            enemyScript.ResetFragment();
        }
    }

    /// <summary>
    /// Enable all clouds in the current level.
    /// </summary>
    private void ResetClouds()
    {
        foreach (GameObject cloud in GameManager.Instance.m_CloudsInTheLevel)
        {
            Cloud cloudScript = cloud.GetComponent<Cloud>();
            cloudScript.Activate();
            cloudScript.ResetFragment();
        }
    }

    private void ResetDangerousSpheres()
    {
        foreach (GameObject dangerousSphere in GameManager.Instance.m_DangerousSpheresInTheLevel)
        {
            DangerousSphere dangerousSphereScript = dangerousSphere.GetComponent<DangerousSphere>();
            dangerousSphereScript.MoveToInitialPosition();
            dangerousSphere.SetActive(true);
        }
    }

    private void ResetTriggers()
    {
        foreach (GameObject trigger in GameManager.Instance.m_TriggersInTheLevel)
        {
            Triggers triggerScript = trigger.GetComponent<Triggers>();
            triggerScript.m_IsMoving = false;
        }
    }

    private void HideProjetileS()
    {

        foreach (GameObject projectile in GameManager.Instance.m_Projectiles)
        {
            Destroy(projectile);
        }
        GameManager.Instance.m_Projectiles.Clear();
    }

    private void ResetTimer()
    {
        Timer timer = GameManager.Instance.m_TimerScript;

        timer.TimerReset();
    }
}
