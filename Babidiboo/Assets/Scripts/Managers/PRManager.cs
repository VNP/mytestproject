﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class PRManager : MonoBehaviour
{
    public Timer m_Time;
    public GameObject m_GameOverUI;
    public Text m_GameOverTime;
    public TextMeshProUGUI m_RemainingEnnemiesValue;


    private static PRManager _instance;

    public static PRManager Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = GameObject.FindObjectOfType<PRManager>();
            }
            return _instance;
        }
    }

    private void Update()
    {

    }

    private void Awake()
    {
        if (_instance == null)
            _instance = this;

        else if (_instance != this)
            Destroy(gameObject);

        DontDestroyOnLoad(gameObject);
    }

    public int GameOverTime()
    {
        int finalTime = 0;

        return finalTime = m_Time.GetTime();
    }

    public void ShowGameOverUI()
    {
        m_GameOverUI.SetActive(true);
        m_GameOverTime.text = ("" + GameOverTime());

        int RemainingEnnemies = GameManager.Instance.GetRemainingEnnemies();
        m_RemainingEnnemiesValue.text = RemainingEnnemies.ToString();
    }

    public void HidGameOverUI()
    {
        m_GameOverUI.SetActive(false);
    }
}
