﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartPosition : MonoBehaviour
{
    public Vector3 m_InitialPosition = Vector3.zero;
    public Quaternion m_InitialRotation = Quaternion.identity;

    private void Awake()
    {
        m_InitialPosition = transform.position;
        m_InitialRotation = transform.rotation;
    }

    public Vector3 GetInitialPosition()
    {
        return m_InitialPosition;
    }

    public void SetInitialRotation()
    {
        transform.rotation =  m_InitialRotation;
    }
}
