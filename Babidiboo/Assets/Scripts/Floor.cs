﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Floor : MonoBehaviour
{
    [SerializeField]
    private Material m_FloorMat;

    private MeshRenderer MR;

    private void Awake()
    {
        MR = GetComponent<MeshRenderer>();
    }

    public void ResetMaterial()
    {
        MR.material = m_FloorMat;
    }
}
