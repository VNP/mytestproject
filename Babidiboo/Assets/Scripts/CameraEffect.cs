﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraEffect : MonoBehaviour
{
    [SerializeField] private float m_FVAdjsuter = 0.05f;

    private const float m_StandardFV = 60;

    private Vector3 m_CameraInitialPosition;

    private void Start()
    {
        m_CameraInitialPosition = Camera.main.transform.localPosition;
    }

    public void AcceleratorFieldOfView(float a_AccCurrentSpeed)
    {
        Camera.main.fieldOfView = m_StandardFV + (m_FVAdjsuter * a_AccCurrentSpeed);
    }

    public void CameraShake(bool a_LoopCondition)
    {
        if(a_LoopCondition)
        {
            Camera.main.transform.localPosition = new Vector3(Random.Range(0f, 0.01f),Random.Range(0f, 0.01f), 0f);
            StartCoroutine(SingleShakeDelay());
        }
    }

    private IEnumerator SingleShakeDelay()
    {
        yield return new WaitForSeconds(0.1f);
    }

    public void ResetCamera()
    {
        Camera.main.transform.localPosition = m_CameraInitialPosition;
    }
}
