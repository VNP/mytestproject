﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallSlide : MonoBehaviour
{
    public bool m_IsTouhcingWallRight = false;
    public bool m_IsTouhcingWallLeft = false;
    public bool m_IsSlidingOnWall = false;
    public bool m_OnCooldown = false;


    public PlayerController m_PlayerController;
    public Rigidbody m_PlayerRB;


    [SerializeField] private float m_SecondsOnCooldown = 1.5f;
    [SerializeField] private LayerMask m_SlidableLayers;



    private Vector3 m_InitialPositionOfContact;

    private const float m_CHECKFORWALLMAXDISTANCE = 0.6f;

    private void Start()
    {
        m_PlayerController = GetComponent<PlayerController>();
        m_PlayerRB = GetComponent<Rigidbody>();
    }

    private void Update()
    {
        if(IsTouchingWall())
        {
            if(!m_OnCooldown)
            {
                if (!m_IsSlidingOnWall && !m_PlayerController.m_IsGrounded && Input.GetButtonDown("Jump"))
                {
                    m_OnCooldown = true;

                    GetFirstPositionOfSlide();

                    m_IsSlidingOnWall = true;
                    m_PlayerRB.useGravity = false;
                    m_PlayerController.m_CanMove = false;
                }
            }
        }

        if(m_PlayerController.m_IsGrounded)
        {
            m_OnCooldown = false;
            StopAllCoroutines();
        }
    }

    private void FixedUpdate()
    {
        if (m_IsSlidingOnWall)
        {
            if (Input.GetButton("Jump") && IsTouchingWall() && !m_PlayerController.m_IsGrounded )
            {
                SlideOnWall();
            }
            else
            {
                StartCoroutine(SlideOnWallCoolDown());
                m_PlayerRB.useGravity = true;
                m_PlayerController.m_CanMove = true;
                m_IsSlidingOnWall = false;
            }
        }
    }

    /// <summary>
    /// Verify if the player is touching a wall on his right or left.
    /// </summary>
    /// <returns></returns>
    private bool IsTouchingWall()
    {
        m_IsTouhcingWallRight = (Physics.Raycast(transform.position, transform.right, m_CHECKFORWALLMAXDISTANCE, m_SlidableLayers));
        Debug.DrawLine(transform.position, transform.position + transform.right * m_CHECKFORWALLMAXDISTANCE, Color.yellow);

        m_IsTouhcingWallLeft = (Physics.Raycast(transform.position, -transform.right, m_CHECKFORWALLMAXDISTANCE, m_SlidableLayers));
        Debug.DrawLine(transform.position, transform.position + -transform.right * m_CHECKFORWALLMAXDISTANCE, Color.yellow);

        if (m_IsTouhcingWallRight || m_IsTouhcingWallLeft)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    /// <summary>
    /// If the player is touching a wall on his right or left and if he is in the air and if he is pressing spacebar, the player slide on the wall.
    private Vector3 GetFirstPositionOfSlide()
    {
            m_InitialPositionOfContact = transform.position;

            return m_InitialPositionOfContact;
    }

    private void SlideOnWall()
    {
        ForceYPositionOnContactWithWall();
        m_PlayerRB.position += (new Vector3(transform.forward.x, 0f, transform.forward.z) * Time.fixedDeltaTime * 10f);
    }

    private void ForceYPositionOnContactWithWall()
    {
        transform.position = new Vector3(transform.position.x, m_InitialPositionOfContact.y, transform.position.z);
    }

    private IEnumerator SlideOnWallCoolDown()
    {
        yield return new WaitForSeconds(m_SecondsOnCooldown);

        m_OnCooldown = false;
    }
}
