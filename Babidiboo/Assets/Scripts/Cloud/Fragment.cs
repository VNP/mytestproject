﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fragment : MonoBehaviour
{
    [SerializeField]
    private List<GameObject> m_FragmentsList;

    public Explosion m_Explosion;

    private StartPosition m_Startposition;

    private void Awake()
    {
        m_Startposition = GetComponent<StartPosition>();
    }

    public void FragmentDisappear()
    {
        foreach (GameObject obj in m_FragmentsList)
        {
            BoxCollider collider = obj.GetComponent<BoxCollider>();
            MeshRenderer meshRenderer = obj.GetComponent<MeshRenderer>();
            Rigidbody RB = obj.GetComponent<Rigidbody>();

            RB.velocity = Vector3.zero;
            RB.angularVelocity = Vector3.zero;
            RB.useGravity = false;
            collider.enabled = false;
            meshRenderer.enabled = false;

        }
        ResetFragmentsPosition();
    }

    public void ResetFragmentsPosition()
    {
        foreach (GameObject obj in m_FragmentsList)
        {
            StartPosition startPosition = obj.GetComponent<StartPosition>();
            obj.transform.position = startPosition.GetInitialPosition();
        }
    }

    public void ResetFragmentsRotation()
    {
        foreach (GameObject obj in m_FragmentsList)
        {
            StartPosition startPosition = obj.GetComponent<StartPosition>();
            startPosition.SetInitialRotation();
        }
    }

    public void DisplayFragments()
    {
        foreach (GameObject obj in m_FragmentsList)
        {
            BoxCollider collider = obj.GetComponent<BoxCollider>();
            MeshRenderer meshRenderer = obj.GetComponent<MeshRenderer>();

            Rigidbody RB = obj.GetComponent<Rigidbody>();
            RB.useGravity = true;

            collider.enabled = true;
            meshRenderer.enabled = true;

        }
    }

    public void DesactivateExplosion()
    {
        m_Explosion.transform.gameObject.SetActive(false);
    }
}
