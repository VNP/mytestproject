﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Animations;
using UnityEngine.Playables;

public class Cloud : MonoBehaviour
{
    public Fragment m_Fragments;

    [SerializeField]
    private PlayableDirector m_PlayableDirector;

    [SerializeField]
    private MeshRenderer m_MeshRenderer;

    [SerializeField]
    private BoxCollider m_Collider;


    private const float m_Delay = 1f;

    private void Awake()
    {
        m_MeshRenderer = GetComponent<MeshRenderer>();
        m_Collider = GetComponent<BoxCollider>();
    }



    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            m_PlayableDirector.Play();
            StartCoroutine(CloudDisappearAfterDelay(m_Delay));
        }
    }

    private IEnumerator CloudDisappearAfterDelay(float a_Delay)
    {
        yield return new WaitForSeconds(a_Delay);
        Desactivate();

        m_Fragments.DisplayFragments();
        m_Fragments.m_Explosion.transform.gameObject.SetActive(true);

        yield return new WaitForSeconds(5f);
        ResetFragment();
    }

    public void ResetFragment()
    {
        StopAllCoroutines();
        m_Fragments.DesactivateExplosion();
        m_Fragments.FragmentDisappear();
        m_Fragments.ResetFragmentsRotation();
        m_Fragments.ResetFragmentsPosition();
    }

    public void Activate()
    {
        m_MeshRenderer.enabled = true;
        m_Collider.enabled = true;
    }

    public void Desactivate()
    {
        m_MeshRenderer.enabled = false;
        m_Collider.enabled = false;
    }

}
