﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public CameraEffect m_CameraEffect;

    public Rigidbody m_PlayerRB;

    public bool m_IsJumping = false;
    public bool m_IsGrounded = false;

    public bool m_IsBoostedByAccelerator = false;

    public bool m_CanReact = true;
    public bool m_CanMove = true;
    public bool m_CanJump = true;

    public float m_MaxVelocity = 0;

    [SerializeField] private float m_Speed = 8f;
    [SerializeField] private float m_SpeedBoost = 1f;
    [SerializeField] private float m_MaxSpeedBoost = 10f;
    [SerializeField] private float m_RotateSpeedHorizontal = 3f;
    [SerializeField] private float m_RotateSpeedVertical = 3f;
    [SerializeField] private float m_JumpPower = 100f;

    [Header("Accelerators")]
    public float m_AccCurrentSpeed = 0f;

    public bool m_AccIsActivated = false;
    public bool m_AccIsGainingSpeed = false;

    public Vector3 m_AcceleratorDirection = new Vector3(0, 0, 0);

    [SerializeField] private float m_AccMaxSpeed = 50f;
    [SerializeField] private float m_AccAcceleration = 50f;
    [SerializeField] private float m_AccDeceleration = 20f;


    [Header("")]
    [SerializeField]private LayerMask m_JumpableLayers;
    [SerializeField] private GameObject m_ProjectilePrefab;
    [SerializeField] private GameObject m_Raycastparent;


    private Vector3 m_CurrentVelocity;
    private Vector3 m_InitialPosition;
    private Vector3 m_InitialRotation;
    private Vector3 m_CheckForGroundDistance;

    private float m_Yaw = 0f;
    private float m_Pitch = 0f;
    private float m_Roll = 0f;

    private const float m_CheckForGroundMaxDistance = 0.8f;
    private const float m_CheckFoWalldMaxDistance = 0.6f;
    private const float m_LeanValue = 5;
    private const float m_MinRotation = -90;
    private const float m_MaxRotation = 90;


    void Start()
    {
        GameManager.Instance.m_PlayerController = this.GetComponent<PlayerController>();

        m_InitialPosition = transform.position;
        m_InitialRotation = transform.eulerAngles;
        SetInitialRotation();

        m_MaxVelocity = 1 * (m_Speed * m_SpeedBoost);

        if (GetComponent<Rigidbody>())
        {
            m_PlayerRB = GetComponent<Rigidbody>();
        }
        else
            Debug.LogError("The player needs a rigidbody.");
    }



    void FixedUpdate()
    {
        if (m_CanReact && m_CanMove)
        {
            float horizontal = Input.GetAxis("Horizontal");
            float vertical = Input.GetAxis("Vertical");

            Move(horizontal, vertical, m_LeanValue);
        }

        if (m_AccIsActivated)
        {
            Accelerate();

            m_CameraEffect.AcceleratorFieldOfView(m_AccCurrentSpeed);
            m_CameraEffect.CameraShake(m_AccIsActivated);
        }
    }

    private void Update()
    {
        if (m_CanReact)
        {
            Shoot(m_ProjectilePrefab);
            Rotate();

            if (m_CanJump)
                Jump();

        }
        if (Input.GetKey(KeyCode.R))
        {
            RestartLevel();
        }
    }

    /// <summary>
    /// Vertical and horizontal movements of the player.
    /// </summary>
    /// <param name="a_horizontal"></param>
    /// <param name="a_vertical"></param>
    private void Move(float a_horizontal, float a_vertical, float a_LeanValue)
    {
        /********************** SPRINT  **********************/
        //if (Input.GetKey(KeyCode.LeftShift))
        //{
        //    if (m_SpeedBoost < m_MaxSpeedBoost)
        //    {
        //        m_SpeedBoost += 3 * Time.deltaTime;

        //        if (m_SpeedBoost > m_MaxSpeedBoost)
        //            m_SpeedBoost = m_MaxSpeedBoost;
        //    }
        //}
        //else
        //{
        //    m_SpeedBoost = 1;
        //}

        if ((a_vertical != 0 || a_horizontal != 0) && !m_IsBoostedByAccelerator)
        {
            Vector3 verticalMovement = new Vector3(transform.forward.x, 0, transform.forward.z);
            Vector3 horizontalMovement = new Vector3(transform.right.x, 0, transform.right.z);

            m_CurrentVelocity = ((verticalMovement * a_vertical) + (horizontalMovement * a_horizontal)) * (m_Speed * m_SpeedBoost);

        }
        else if (m_IsBoostedByAccelerator)
        {
            if (a_horizontal != 0)
            {
                Vector3 horizontalMovement = new Vector3(transform.right.x, 0, transform.right.z);

                m_CurrentVelocity = (horizontalMovement * a_horizontal) * (m_Speed * m_SpeedBoost);
                m_CurrentVelocity.y = m_PlayerRB.velocity.y;
                m_PlayerRB.velocity = m_CurrentVelocity;
            }
        }
        else
        {
            m_CurrentVelocity = Vector3.zero;
        }

        m_CurrentVelocity.y = m_PlayerRB.velocity.y;
        //m_CurrentVelocity *= m_Speed;
        m_PlayerRB.velocity = m_CurrentVelocity;

        m_Roll = (a_horizontal * a_LeanValue) * -1;
    }

    /// <summary>
    /// The rotation of the player.
    /// </summary>
    private void Rotate()
    {
        {
            m_Yaw += m_RotateSpeedHorizontal * Input.GetAxis("Mouse X");
            m_Pitch -= m_RotateSpeedVertical * Input.GetAxis("Mouse Y");

            m_Pitch = Mathf.Clamp(m_Pitch, m_MinRotation, m_MaxRotation);

            transform.eulerAngles = new Vector3(m_Pitch, m_Yaw, m_Roll);
        }
    }

    /// <summary>
    /// It makes the player jump.
    /// </summary>

    private void Jump()
    {
        CheckForGround();

        if (Input.GetButtonDown("Jump") && m_IsGrounded)
        {
            m_IsGrounded = false;

            Vector3 currentVelocity = m_PlayerRB.velocity;
            currentVelocity.y = 0;
            m_PlayerRB.velocity = currentVelocity;

            m_PlayerRB.AddForce(m_JumpPower * Vector3.up);
        }
    }

    /// <summary>
    /// Verify if there is a groundLayer under the player. 
    /// </summary>
    private void CheckForGround()
    {
        //m_IsGrounded = (Physics.Raycast(transform.position, Vector3.down, m_CheckForGroundMaxDistance, m_JumpableLayers));
        //Debug.DrawLine(transform.position, transform.position + Vector3.down * m_CheckForGroundMaxDistance, Color.red);

        if (m_PlayerRB.velocity.y < 0)
        {
            for (int i = 0; i < m_Raycastparent.transform.childCount; i++)
            {
                Debug.DrawRay(m_Raycastparent.transform.GetChild(i).transform.position, -transform.up);
                if (Physics.Raycast(m_Raycastparent.transform.GetChild(i).transform.position, -transform.up, 0.6f))
                {
                    m_IsGrounded = true;
                    return;
                }
            }
        }
    }

    /// <summary>
    /// Allow the player to shoot if he press the fire button.
    /// </summary>
    private void Shoot(GameObject a_Projectile)
    {
        if (Input.GetButtonDown("Fire1"))
        {
            Projectile projectileScript;
            projectileScript = a_Projectile.GetComponent<Projectile>();
            projectileScript.Shooter = gameObject;

            Instantiate(a_Projectile, transform.position, transform.rotation);
        }
    }

    public void RestartLevel()
    {
        RestartManager.Instance.RestartLevel();

        SetInitialPosition();
        SetInitialRotation();

        m_PlayerRB.isKinematic = false;
        m_CanReact = true;
    }

    private void SetInitialPosition()
    {
        transform.position = m_InitialPosition;
    }

    private void SetInitialRotation()
    {
        m_Yaw = m_InitialRotation.y;
        m_Pitch = m_InitialRotation.x;
        transform.localEulerAngles = m_InitialRotation;
    }

    /// <summary>
    /// Called when the player touch an accelerator.
    /// </summary>
    private void Accelerate()
    {
        if (!m_AccIsActivated)
            return;

        if (m_AccIsGainingSpeed)
        {
            m_AccCurrentSpeed += m_AccAcceleration * Time.fixedDeltaTime;

            if (m_AccCurrentSpeed >= m_AccMaxSpeed)
            {
                m_AccCurrentSpeed = m_AccMaxSpeed;

                m_AccIsGainingSpeed = false;
            }
        }

        //Prevent m_AccCurrentSpeed to go beyond its max value if the player touch two accelerators in a row.
        if (m_AccCurrentSpeed >= m_AccMaxSpeed)
        {
            m_AccCurrentSpeed = m_AccMaxSpeed;
        }

        if (!m_AccIsGainingSpeed)
        {
            m_AccCurrentSpeed -= m_AccDeceleration * Time.fixedDeltaTime;

            if (m_AccCurrentSpeed <= 0 || (m_AccCurrentSpeed <= 10 && Input.GetAxis("Vertical") > 0.01f))
            {
                m_AccIsActivated = false;
                m_IsBoostedByAccelerator = false;

                m_AccCurrentSpeed = 0;

                m_CameraEffect.ResetCamera();
            }
        }
        m_PlayerRB.position += (m_AcceleratorDirection * Time.deltaTime * m_AccCurrentSpeed);
    }

    private void RaycastGround()
    {
        if (m_PlayerRB.velocity.y < 0)
        {
            for (int i = 0; i < m_Raycastparent.transform.childCount; i++)
            {
                Debug.DrawRay(m_Raycastparent.transform.GetChild(i).transform.position, -transform.up);
                if (Physics.Raycast(m_Raycastparent.transform.GetChild(i).transform.position, -transform.up, 0.6f))
                {
                    m_IsGrounded = true;
                    return;
                }
            }
        }
    }
}

