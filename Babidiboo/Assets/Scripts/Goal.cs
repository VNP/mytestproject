﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Goal : MonoBehaviour
{

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Player")
        {
            Time.timeScale = 0;

            PlayerController playerScript = other.gameObject.GetComponent<PlayerController>();
            playerScript.m_CanReact = false;

            Rigidbody playerRibidbody = playerScript.m_PlayerRB;
            playerRibidbody.isKinematic = true;

            VictoryCondition();

            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;
        }
    }

    /// <summary>
    /// Verify if the victory condition is met.
    /// </summary>
    private void VictoryCondition()
    {
        VerifyEnemiesState();

        if (GameManager.Instance.remainingEnemies == 0)
        {
            GameManager.Instance.areAllEnemiesDead = true;
            PRManager.Instance.ShowGameOverUI();

            Debug.Log("All Enemies are dead.");
        }
        else
        {
            PRManager.Instance.ShowGameOverUI();
            Debug.Log("Some enemies are alive");
        }

        Debug.Log("You've completed the level in: " + PRManager.Instance.GameOverTime());
        
    }

    /// <summary>
    /// Verify if all enemies are alive.
    /// </summary>
    private void VerifyEnemiesState()
    {
        foreach (GameObject enemy in GameManager.Instance.m_EnnemiesInTheLevel)
        {
            Enemy enemyscript = enemy.gameObject.GetComponent<Enemy>();

            if (enemyscript.m_IsAlive)
            {
                GameManager.Instance.remainingEnemies++;
            }
        }
    }
}
