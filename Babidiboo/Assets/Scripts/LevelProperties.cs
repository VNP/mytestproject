﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelProperties : MonoBehaviour
{
    [SerializeField] private int m_LevelId;

    //Gooes in GameManager
    [Header("Interactive Elements")]
    [SerializeField] private List<GameObject> m_AllEnemies = new List<GameObject>();
    [SerializeField] private List<GameObject> m_AllClouds = new List<GameObject>();
    [SerializeField] private List<GameObject> m_AllDangerousSphere = new List<GameObject>();
    [SerializeField] private List<GameObject> m_AllTrigger = new List<GameObject>();
    [SerializeField] private List<GameObject> m_AllFloors = new List<GameObject>();
    [SerializeField] private Text m_Timer;

    //Goes in PRManager
    [SerializeField] private Timer m_Time;
    [SerializeField] private GameObject m_GameOverUI;
    [SerializeField] private Text m_GameOverTime;

    void Start()
    {
        //Set GameManager
        SetAllFloorsInGameManager();
        SetAllEnemiesInGameManager();
        SetAllCloudsInGameManager();
        SetAllDangerousSphereInGameManager();
        SetAllTriggersInGameManager();
        SetTimer();

        //Set PRManager
        SetGameOverUI();
        SetGameOverTime();
    }

    private void SetAllFloorsInGameManager()
    {
        foreach (GameObject floor in m_AllFloors)
        {
            GameManager.Instance.m_FloorsInTheLevel.Add(floor);
        }
    }

    private void SetAllEnemiesInGameManager()
    {
        foreach(GameObject enemy in m_AllEnemies)
        {
            GameManager.Instance.m_EnnemiesInTheLevel.Add(enemy);
        }
    }

    private void SetAllCloudsInGameManager()
    {
        foreach (GameObject cloud in m_AllClouds)
        {
            GameManager.Instance.m_CloudsInTheLevel.Add(cloud);
        }
    }

    private void SetAllDangerousSphereInGameManager()
    {
        foreach (GameObject sphere in m_AllDangerousSphere)
        {
            GameManager.Instance.m_DangerousSpheresInTheLevel.Add(sphere);
        }
    }

    private void SetAllTriggersInGameManager()
    {
        foreach (GameObject trigger in m_AllTrigger)
        {
            GameManager.Instance.m_TriggersInTheLevel.Add(trigger);
        }
    }

    private void SetTimer()
    {
        GameManager.Instance.m_Timer = m_Timer;
        PRManager.Instance.m_Time = m_Time;
    }

    private void SetGameOverUI()
    {
        PRManager.Instance.m_GameOverUI = m_GameOverUI;
    }

    private void SetGameOverTime()
    {
        PRManager.Instance.m_GameOverTime = m_GameOverTime;
    }
}
