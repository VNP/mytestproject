﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Accelerator : MonoBehaviour
{
    private PlayerController m_PlayerController;
    private Vector3 m_TransformForward;

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player")
        {
            m_PlayerController = other.GetComponent<PlayerController>();

            if(m_PlayerController.m_AccIsActivated)
            {
                m_PlayerController.m_AccCurrentSpeed *= 2;
                m_PlayerController.m_AcceleratorDirection = transform.forward;
            }
            else
            {
                m_PlayerController.m_AccIsActivated = true;
                m_PlayerController.m_IsBoostedByAccelerator = true;
                m_PlayerController.m_AccIsGainingSpeed = true;

                m_PlayerController.m_AcceleratorDirection = transform.forward;
            }
        }
    }
}
