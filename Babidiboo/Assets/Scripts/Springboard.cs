﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Springboard : MonoBehaviour
{
    [SerializeField] private float m_SpringPower = 100f;

    private Vector3 m_Velocity = new Vector3(0f, 0f, 0f);

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            PlayerController playerScript = other.GetComponent<PlayerController>();

            Rigidbody playerRB = other.GetComponent<Rigidbody>();

            PlayerController playerController = other.GetComponent<PlayerController>();

            m_Velocity.x = playerRB.velocity.x;
            m_Velocity.z = playerRB.velocity.z;

            playerController.m_IsGrounded = false;

            playerRB.velocity = m_Velocity;

            playerRB.AddForce(m_SpringPower * Vector3.up);

            playerScript.m_IsGrounded = false;
        }
    }
}
