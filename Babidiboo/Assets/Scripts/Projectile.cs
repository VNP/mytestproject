﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    public GameObject Shooter;

    public float m_Speed = 2f;

    public float m_AdditionalSpeed = 2f;

    private float m_SecondsBeforeDestroy = 4f;

    [SerializeField] private bool m_IsPlayerAccelerating = false;

    [SerializeField] private PlayerController m_PlayerController;

    [SerializeField] private bool m_IsEnemyProjectile = false;
    [SerializeField] private GameObject m_Particle;

    void Start()
    {
        m_PlayerController = GameManager.Instance.m_PlayerController;
        m_IsPlayerAccelerating = m_PlayerController.m_AccIsActivated;

        if (Shooter.tag == "Enemy")
        {
            m_IsEnemyProjectile = true;
        }

        StartCoroutine(TimeBeforeDestroy(m_SecondsBeforeDestroy));
        GameManager.Instance.m_Projectiles.Add(gameObject);
    }
    void FixedUpdate()
    {
        m_AdditionalSpeed = m_PlayerController.m_AccCurrentSpeed * 1f;

        if (m_IsPlayerAccelerating)
        {
            transform.Translate(Vector3.forward * (m_Speed + m_AdditionalSpeed) * Time.fixedDeltaTime);
        }
        else
            transform.Translate(Vector3.forward * (m_Speed) * Time.fixedDeltaTime);
    }

    /// <summary>
    /// Delete this after x seconds.
    /// </summary>
    /// <param name="seconds"></param>
    /// <returns></returns>
    private IEnumerator TimeBeforeDestroy(float seconds)
    {
        yield return new WaitForSeconds(seconds);
        GameManager.Instance.m_Projectiles.Remove(gameObject);
        Destroy(this.transform.gameObject);
    }

    /// <summary>
    /// Desactivate the ennemy on collision and delete the projectile.
    /// </summary>
    /// <param name="other"></param>
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Enemy" && !m_IsEnemyProjectile)
        {
            Enemy enemyScript = other.gameObject.GetComponent<Enemy>();
            enemyScript.OnDeath();

            GameManager.Instance.m_Projectiles.Remove(gameObject);
            Destroy(this.transform.gameObject);
        }
        // If this is an enemy projectile && it touched the player, the level restart.
        else if (other.gameObject.tag == "Player" && m_IsEnemyProjectile)
        {
            PlayerController player;
            player = other.GetComponent<PlayerController>();
            player.RestartLevel();
        }

        else if (other.gameObject.tag == "Projectile" && other.gameObject.GetComponent<Projectile>().m_IsEnemyProjectile)
        {
            Projectile projectileScript = other.GetComponent<Projectile>();
            GameObject projectileEmiter = projectileScript.Shooter;

            //other.gameObject.SetActive(false);

            Enemy enemyScript = projectileEmiter.GetComponent<Enemy>();
            enemyScript.OnDeath();

            GameManager.Instance.m_Projectiles.Remove(gameObject);
            Destroy(this.transform.gameObject);
        }

        else if (other.gameObject.tag == "EvilBall")
        {
            other.gameObject.SetActive(false);

            GameManager.Instance.m_Projectiles.Remove(gameObject);
            Destroy(this.transform.gameObject);
        }

        else
        {
            Instantiate(m_Particle, transform.position, Quaternion.identity);
            GameManager.Instance.m_Projectiles.Remove(gameObject);
            Destroy(this.gameObject);
        }
    }
}