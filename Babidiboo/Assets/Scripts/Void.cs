﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Void : MonoBehaviour
{

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            PlayerController playerScript = other.GetComponent<PlayerController>();
            playerScript.RestartLevel();
        }
    }
}
