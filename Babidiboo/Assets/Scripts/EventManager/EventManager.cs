﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventManager : MonoBehaviour
{

    public static EventManager Instance { get; private set; }

    private Dictionary<EventId, System.Action<object>> m_EventDict;

    private void Awake()
    {
        if(Instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            Instance = this;
        }

        m_EventDict = new Dictionary<EventId, System.Action<object>>();
    }

    public void RegisterEvent(EventId aId, System.Action<object> aCallback)
    {
        if(m_EventDict.ContainsKey(aId))
        {
            m_EventDict[aId] += aCallback;
        }
        else
        {
            m_EventDict.Add(aId, aCallback);
        }
    }

    public void UnregisterEvent(EventId aId, System.Action<object> aCallback)
    {
        if (m_EventDict.ContainsKey(aId))
        {
            m_EventDict[aId] -= aCallback;
        }
        else
        {
            Debug.Log("Trying to unregister a method not registered.");
        }
    }

    public void Dispatch(EventId aId, object aArag = null)
    {
        if (m_EventDict.ContainsKey(aId))
        {
            m_EventDict[aId](aArag);
        }
        else
        {
            Debug.LogError("Trying to dispatch an unregistered EventId.");
        }
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
