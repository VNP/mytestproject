﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelBtn : MonoBehaviour
{

    [SerializeField] private string m_SceneName;

    public void OnClick()
    {
        SceneManager.LoadScene(m_SceneName);
    }

}
