﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Logo : MonoBehaviour
{
    [SerializeField] private GameObject BaseBtns;

    public Image img;

    public void Start()
    {

        StartCoroutine(FadeIn());

    }

    IEnumerator FadeIn()
    {

        for (float i = 0; i <= 1; i += Time.deltaTime)
        {
            img.color = new Color(1, 1, 1, i);
            yield return null;
        }

        yield return new WaitForSeconds(2f);

        for (float i = 1; i >= 0; i -= Time.deltaTime)
        {
            img.color = new Color(1, 1, 1, i);
            yield return null;
        }

        ActivateMenu();
        this.gameObject.SetActive(false);
        yield return null;
    }

    private void ActivateMenu()
    {
        BaseBtns.SetActive(true);
    }

}
