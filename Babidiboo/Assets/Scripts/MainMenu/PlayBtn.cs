﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayBtn : MonoBehaviour
{
    [SerializeField] private GameObject m_LevelSelection;
    [SerializeField] private GameObject m_QuitBtn;

    public void OnClick()
    {
        m_LevelSelection.SetActive(true);
        m_QuitBtn.SetActive(false);
        this.gameObject.SetActive(false);
    }

}
