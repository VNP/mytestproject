﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    [SerializeField] private const float m_ShootDelay = 1.5f;
    [SerializeField] private GameObject m_Projectile;

    private PlayerController m_Player;
    private GameObject m_Target;

    public LayerMask m_LayerToIgnore;
    public MeshRenderer m_MeshRenderer;
    public BoxCollider m_BoundingBox;
    public Fragment m_Fragments;

    public bool m_IsAlive = true;

    [SerializeField] private Material m_AEIMouth_mat;
    [SerializeField] private Material m_BMPMouth_mat;
    [SerializeField] private Material m_O_mat;
    [SerializeField] private Material m_CDGKNSTXYZMouth_mat;

    private MeshRenderer m_MesRenderer;

    public bool m_IsOnCooldown;

    private void OnEnable()
    {
        m_MesRenderer.material = m_AEIMouth_mat;
    }

    private void Awake()
    {
        m_MesRenderer = GetComponent<MeshRenderer>();
    }
    void Start()
    {
        m_Player = FindObjectOfType<PlayerController>();
        m_Target = m_Player.transform.gameObject;


        m_IsOnCooldown = false;
    }

    void Update()
    {
        EnemyBehaviours();
    }

    /// <summary>
    /// The behaviours of the enemy.
    /// </summary>
    private void EnemyBehaviours()
    {
        if (SeeAndLookAtThePlayer())
        {
            if (!m_IsOnCooldown)
            {
                m_IsOnCooldown = !m_IsOnCooldown;

                StartCoroutine(DelayInSeconds(m_ShootDelay));
            }
        }
    }

    private IEnumerator DelayInSeconds(float a_ShootDelay)
    {
        // Ba
        yield return new WaitForSeconds(0.08f);
        m_MesRenderer.material = m_BMPMouth_mat;
        yield return new WaitForSeconds(0.08f);
        m_MesRenderer.material = m_AEIMouth_mat;

        // Bi
        yield return new WaitForSeconds(0.08f);
        m_MesRenderer.material = m_BMPMouth_mat;
        yield return new WaitForSeconds(0.08f);
        m_MesRenderer.material = m_AEIMouth_mat;

        // Di
        yield return new WaitForSeconds(0.08f);
        m_MesRenderer.material = m_CDGKNSTXYZMouth_mat;
        yield return new WaitForSeconds(0.08f);
        m_MesRenderer.material = m_AEIMouth_mat;
        // Boo
        yield return new WaitForSeconds(0.08f);
        m_MesRenderer.material = m_BMPMouth_mat;
        yield return new WaitForSeconds(0.08f);
        m_MesRenderer.material = m_O_mat;


        if(m_IsAlive)
            Shoot(m_Projectile);

        yield return new WaitForSeconds(a_ShootDelay);
            
            m_IsOnCooldown = !m_IsOnCooldown;

    }

    /// <summary>
    /// The enemy look at the player if there is no obstructions between them. 
    /// </summary>
    /// <returns></returns>
    private bool SeeAndLookAtThePlayer()
    {
        RaycastHit hit;
        Vector3 rayDirection = m_Target.transform.position - transform.position;

        if(Physics.Raycast(transform.position, rayDirection, out hit, LayerMask.GetMask("Player")))
        {
                transform.LookAt(m_Target.transform);

                return true;
        }
        return false;
    }
    
    private void Shoot(GameObject a_Projectile)
    {
        Projectile projectileScript;

        projectileScript = a_Projectile.GetComponent<Projectile>();
        projectileScript.Shooter = gameObject;


        Instantiate(a_Projectile, transform.position + transform.forward, transform.rotation);
    }

    public void OnDeath()
    {
        m_IsAlive = false;
        m_MeshRenderer.enabled = false;
        m_BoundingBox.enabled = false;

        m_Fragments.DisplayFragments();
        StartCoroutine(HideFragments());
        m_Fragments.m_Explosion.gameObject.SetActive(true);

        this.enabled = false;
    }

    public void ResetFragment()
    {
        m_Fragments.DesactivateExplosion();
        m_Fragments.FragmentDisappear();
        m_Fragments.ResetFragmentsRotation();
        m_Fragments.ResetFragmentsPosition();
    }

    private IEnumerator HideFragments()
    {
        yield return new WaitForSeconds(3f);
        m_Fragments.FragmentDisappear();
    }

}
