﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Triggers : MonoBehaviour
{
    public bool m_IsMoving = false;

    [SerializeField] private float m_Speed;

    [SerializeField] private bool m_IsAMovingSphere;

    [SerializeField] private GameObject m_ObjectReacting;

    [SerializeField] private GameObject m_Waypoint;



    private void OnTriggerEnter(Collider other)
    {
        if (m_IsAMovingSphere && other.tag == "Player")
        {
            m_IsMoving = true;
        }
    }

    private void Update()
    {
        if (m_IsMoving)
            m_ObjectReacting.transform.position = Vector3.Lerp(m_ObjectReacting.transform.position, m_Waypoint.transform.position, 0.2f * m_Speed * Time.deltaTime);
    }
}
